megachat
=====

An OTP application - simple chat over Websocket

Build and run (debug)
-----

    $ rebar3 shell

Client part (debug)
----

Point Your browser to [URL](http://localhost:8080/) and try. See console log of browser.

Client API over Websocket
----

Register user in room:
```
{
    "type":"register",
    "user":"John",
    "room":"Room1"
}
```
Response:
```
{
    "type":"register",
    "status":"ok",
    "users":["John"],
    "messages":[{"user":"John","time":1574421164377,"text":"Hello World!"},{"user":"John","time":1574421165152,"text":"Hello World!"}]
}
```

Send message:
```
{
    "type":"message",
    "text":"This is a message text"
}
```

Incoming message:
```
{
    "type":"message",
    "message":{
        "user":"John",
        "time":1574421166219,
        "text":"Hello World!"
    }
}
```

Incoming notification about new user:
```
{
    "user":"John",
    "type":"add_user"
}
```
Incoming notification about detached user:
```
{
    "user":"John",
    "type":"del_user"
}
```
