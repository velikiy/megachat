-module(megachat_ws).
-behaviour(cowboy_websocket).

-export([init/2, websocket_init/1, websocket_handle/2, websocket_info/2, terminate/3]).

init(Req, State) ->
    lager:info("websocket upgrade..."),
    {cowboy_websocket, Req, State, #{idle_timeout => 60000}}.

websocket_init(_State) ->
    lager:info("websocket upgrade completed"),
    {ok, #{}}.

websocket_handle({text, Req}, State) ->
    Obj = jiffy:decode(Req, [return_maps]),
    lager:info("WS recvd: ~p", [Obj]),
    case Obj of
        #{<<"type">> := <<"register">>, <<"room">> := Room, <<"user">> := User} ->
            case catch gproc:reg({n, l, {user, User}}) of
                true ->
                    gproc:reg({r, l, {room, Room}}, User),
                    gproc:send({r, l, {room, Room}}, {add_user, User}),
                    Users = [User1 || {_Pid, User1} <- gproc:lookup_values({r, l, {room, Room}})],
                    Resp = #{
                        type => register,
                        status => ok,
                        users => Users,
                        messages => megachat_cache:get_messages(Room)
                    },
                    {[{text, jiffy:encode(Resp)}], State#{user => User, room => Room}};
                _ ->
                    Resp = #{
                        type => register,
                        status => error,
                        error_message => <<"Name already in use">>
                    },
                    {[{text, jiffy:encode(Resp)}, {close, 1000, <<"Name already in use">>}], State}
            end;
        #{<<"type">> := <<"message">>, <<"text">> := Text} ->
            Msg = #{
                user => maps:get(user, State),
                time => erlang:system_time(millisecond),
                text => Text
            },
            Room = maps:get(room, State),
            lager:info("Message: ~s/~p", [Room, Msg]),
            megachat_cache:save_message(Room, Msg),
            gproc:send({r, l, {room, maps:get(room, State)}}, {msg, Msg}),
            {ok, State}

    end;
websocket_handle(_Frame, State) ->
    {ok, State}.

websocket_info({del_user, User}, State) ->
    Resp = #{
        type => del_user,
        user => User
    },
    {[{text, jiffy:encode(Resp)}], State};
websocket_info({add_user, User}, State) ->
    Resp = #{
        type => add_user,
        user => User
    },
    {[{text, jiffy:encode(Resp)}], State};
websocket_info({msg, Msg}, State) ->
    Resp = #{
        type => message,
        message => Msg
    },
    {[{text, jiffy:encode(Resp)}], State};
websocket_info(_Info, State) ->
    {ok, State}.

terminate(_, _Req, _State = #{room := Room, user := User}) ->
    gproc:unreg({r, l, {room, Room}}),
    gproc:unreg({n, l, {user, User}}),
    gproc:send({r, l, {room, Room}}, {del_user, User}),
    lager:info("detaching ~s/~s", [Room, User]),
    ok;
terminate(_, _Req, _State) -> ok.

