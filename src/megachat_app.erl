%%%-------------------------------------------------------------------
%% @doc megachat public API
%% @end
%%%-------------------------------------------------------------------

-module(megachat_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    ets:new(megachat_cache, [named_table, ordered_set, public]),
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/", cowboy_static, {priv_file, megachat, "static/index.html"}},
            {"/ws", megachat_ws, []}
        ]}
    ]),
    {ok, Port} = application:get_env(megachat, port),
    {ok, _} = cowboy:start_clear(my_http_listener,
        [{port, Port}],
        #{env => #{dispatch => Dispatch}}
    ),
    megachat_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
