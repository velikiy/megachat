-module(megachat_cache).
-behaviour(gen_server).
-include_lib("stdlib/include/qlc.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

-export([start_link/1, init/1, handle_call/3, handle_cast/2, handle_info/2, save_message/2, get_messages/1]).

start_link(Timeout) ->
    gen_server:start_link({local, megachat_cache}, ?MODULE, [Timeout], []).

save_message(Room, #{user := User, time := Time, text := Text}) ->
    ets:insert(megachat_cache, {Time, Room, User, Text}).

get_messages(Room) ->
    QH = qlc:q([#{
        time => Time,
        user => User,
        text => Text
        } || {Time, Room1, User, Text} <- ets:table(megachat_cache), Room == Room1]),
    qlc:e(QH).

init([Timeout]) ->
    erlang:send_after(Timeout, self(), flush),
    {ok, Timeout}.

handle_call(_Req, _From, State) ->
    {reply, ok, State}.

handle_cast(_Req, State) ->
    {noreply, State}.

handle_info(flush, State = Timeout) ->
    FlushTime = erlang:system_time(millisecond) - Timeout,
    MS = ets:fun2ms(fun
        ({Time, _Room, _User, _Text}) when Time < FlushTime ->
            true
    end),
    lager:info("ETS before cleaning: ~p", [ets:tab2list(megachat_cache)]),
    ets:select_delete(megachat_cache, MS),
    erlang:send_after(Timeout, self(), flush),
    {noreply, State};
handle_info(_Info, State) ->
    {noreply, State}.

